ics-ans-role-ca-gateway
===================

Ansible role to install ca-gateway.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
# Gateway installation details
ca_gateway_version:
ca_gateway_archive:
ca_gateway_home:
ca_gateway_directory:
ca_gateway_path:

# Epics base shared libraries
ca_gateway_epics_base_version:
ca_gateway_epics_base_lib_path:
ca_gateway_epics_base_lib_archive:

# Service parameters and variables
ca_gateway_service_log:
ca_gateway_service_archive:
ca_gateway_service_signore:
ca_gateway_service_access:
ca_gateway_service_prefix:
ca_gateway_service_no_cache:
ca_gateway_service_max_array_bytes:
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ca-gateway
```

License
-------

BSD 2-clause
