import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_gateway_enabled_and_running(Service):
    service = Service('cagateway')
    assert service.is_enabled
    assert service.is_running
